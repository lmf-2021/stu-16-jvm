import java.util.Date;

/**
 * 字节码文件中方法内部剖析：
 */
public class LocalVariablesTest {

    public static void main(String[] args) {
        testStatic();
    }

    public static void testStatic(){
        Date date = new Date();
        int count = 10;
        System.out.println(count);
    }
}
