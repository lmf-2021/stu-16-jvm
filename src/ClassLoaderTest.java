import sun.net.spi.nameservice.dns.DNSNameService;

/**
 * 查看各种类加载器
 */
public class ClassLoaderTest {
//    static int num2;
    private static int num = 10;
    static {
        num = 20;
        num2 = 40;
//        System.out.println(num2);
    }
    private static int num2 = 30;

    public static void main(String[] args) {
        System.out.println(ClassLoaderTest.num);//20
        System.out.println(ClassLoaderTest.num2);//30
        System.out.println("通过类加载器类，获取系统类加载器："+ClassLoader.getSystemClassLoader());
        System.out.println("通过自定义类，获取加载自定义类的类加载器："+new ClassLoaderTest().getClass().getClassLoader());
        System.out.println("通过自定义类，获取加载自定义类的类加载器的上一级类加载器（又叫，扩展类加载器）："+new ClassLoaderTest().getClass().getClassLoader().getParent());

        try {
            System.out.println(new DNSNameService().getClass().getClassLoader());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
